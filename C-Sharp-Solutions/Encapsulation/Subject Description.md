﻿# Encapsulation v1.0
[![Codeline|Academy](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

## <tr version>
> Bir Nesnenin bazi özellik ve işlevlerini diğer sınıflardan ve nesnelerden saklanması gereklidir.
> Kapsülleme (Encapsulation) sayesinde nesneler bilinçsiz kullanımdan korunmuş olur

